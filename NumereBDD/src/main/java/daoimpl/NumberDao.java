package daoimpl;

import java.util.List;
import java.util.Optional;

import javax.persistence.TypedQuery;

import dao.Dao;
import helper.DatabaseHelper;
import model.Number;

// TODO: Auto-generated Javadoc
/**
 * The Class NumberDao.
 */
public class NumberDao implements Dao<Number> {

	/** The database helper. */
	private DatabaseHelper databaseHelper;

	/**
	 * Instantiates a new number dao.
	 *
	 * @param databaseHelper the database helper
	 */
	public NumberDao(DatabaseHelper databaseHelper) {
		this.databaseHelper = databaseHelper;
	}

	/**
	 * Gets the.
	 *
	 * @param id the id
	 * @return the optional
	 */
	@Override
	public Optional<model.Number> get(int id) {
		return Optional.ofNullable(databaseHelper.getEntityManager().find(model.Number.class, id));
	}

	/**
	 * Gets the all.
	 *
	 * @return the all
	 */
	@Override
	public List<model.Number> getAll() {
		TypedQuery<model.Number> query = databaseHelper.getEntityManager().createQuery("SELECT a from Number a", model.Number.class);
		return query.getResultList();
	}

	/**
	 * Creates the.
	 *
	 * @param number the number
	 * @return true, if successful
	 */
	@Override
	public boolean create(model.Number number) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.persist(number));
	}

	/**
	 * Update.
	 *
	 * @param old the old
	 * @param newObj the new obj
	 * @return true, if successful
	 */
	@Override
	public boolean update(model.Number old, model.Number newObj) {
		old.setNumber(newObj.getNumber());

		return databaseHelper.executeTransaction(entityManager -> entityManager.merge(old));
	}

	/**
	 * Delete.
	 *
	 * @param number the number
	 * @return true, if successful
	 */
	@Override
	public boolean delete(model.Number number) {
		return databaseHelper.executeTransaction(entityManager -> entityManager.remove(number));
	}

}
