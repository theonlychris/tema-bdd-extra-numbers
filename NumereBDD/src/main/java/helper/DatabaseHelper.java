package helper;

import java.util.function.Consumer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

// TODO: Auto-generated Javadoc
/**
 * The Class DatabaseHelper.
 */
public class DatabaseHelper {
	
	/**
	 * Instantiates a new database helper.
	 */
	private DatabaseHelper() {
		
	}
	
	/** The prim instance. */
	private static DatabaseHelper primInstance;
	
	/** The par instance. */
	private static DatabaseHelper parInstance;
	
	/** The impar instance. */
	private static DatabaseHelper imparInstance;
	
	/**
	 * Gets the single instance of DatabaseHelper.
	 *
	 * @param instanceName the instance name
	 * @return single instance of DatabaseHelper
	 */
	public static DatabaseHelper getInstance(String instanceName) {
		
		switch(instanceName){
		
		case "Prim": {
			if (primInstance == null) {
				primInstance = new DatabaseHelper();
				primInstance.init("Prime");
			}
			return primInstance;
		}
		
		case "Par": {
			if (parInstance == null) {
				parInstance = new DatabaseHelper();
				parInstance.init("Pare");
			}
			return parInstance;
		}
		
		case "Impar": {
			if (imparInstance == null) {
				imparInstance = new DatabaseHelper();
				imparInstance.init("Impare");
			}
			return imparInstance;
		}
			
		}
		return null;
	}
	
	/** The entity manager factory. */
	private EntityManagerFactory entityManagerFactory;
	
	/** The entity manager. */
	private EntityManager entityManager;

	/**
	 * Gets the entity manager.
	 *
	 * @return the entity manager
	 */
	public EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Execute transaction.
	 *
	 * @param action the action
	 * @return true, if successful
	 */
	public boolean executeTransaction(Consumer<EntityManager> action) {
		EntityTransaction entityTransaction = entityManager.getTransaction();
		try {
			entityTransaction.begin();
			action.accept(entityManager);
			entityTransaction.commit();
		} catch (RuntimeException e) {
			System.err.println("Transaction error: " + e.getLocalizedMessage());
			entityTransaction.rollback();
			return false;
		}

		return true;
	}

	/**
	 * Inits the.
	 *
	 * @param instanceName the instance name
	 * @return true, if successful
	 */
	private boolean init(String instanceName) {
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory(instanceName);
			entityManager = entityManagerFactory.createEntityManager();
		} catch (Exception e) {
			System.err.println("Error at initialing DatabaseManager: " + e.getMessage().toString());
			return false;
		}

		return true;
	}
}
