package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

import dao.Dao;
import daoimpl.NumberDao;
import model.Number;
import helper.DatabaseHelper;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public class Main {

	/** The prim instance. */
	private static DatabaseHelper primInstance = DatabaseHelper.getInstance("Prim");
	
	/** The par instance. */
	private static DatabaseHelper parInstance = DatabaseHelper.getInstance("Par");
	
	/** The impar instance. */
	private static DatabaseHelper imparInstance = DatabaseHelper.getInstance("Impar");
	
	/** The prim dao. */
	private static NumberDao primDao = new NumberDao(primInstance);
	
	/** The par dao. */
	private static NumberDao parDao  = new NumberDao(parInstance);
	
	/** The impar dao. */
	private static NumberDao imparDao = new NumberDao(imparInstance);

	/**
	 * Prints the list.
	 *
	 * @param <T> the generic type
	 * @param list the list
	 */
	public static <T> void printList(List<T> list) {
		// Display array elements
		for (T element : list) {
			System.out.printf("%s ", element);
			System.out.println();
		}
		System.out.println();
	}

	/**
	 * Checks if is prim.
	 *
	 * @param number the number
	 * @return true, if is prim
	 */
	public static boolean isPrim(int number) {
		for (int i = 2; i < number; i++) {
			if (number % i == 0)
				return false;
		}
		return true;

	}
	
	/**
	 * Checks if is par.
	 *
	 * @param number the number
	 * @return true, if is par
	 */
	public static boolean isPar(int number) {
		return number % 2 == 0 ? true : false;
	}

	
	/**
	 * Menu.
	 */
	private static void Menu() {

		Scanner scanner = new Scanner(System.in);

		while (true) {
			showPageOne();

			int option = scanner.nextInt();

			switch (option) {
			case 1:
				addNewNumber();
				break;
			case 2:
				printAll();
				break;
			/*
			case 3:
				showPrimeNumbers();
				break;
			case 4:
				showParNumbers();
				 break;
			case 5:
				showImparNumbers();
				break;
			*/
			default:
				option = -1;
				break;
			}

			if (option == -1)
				break;
		}

	}

	/**
	 * Gets the number type.
	 *
	 * @param number the number
	 * @return the number type
	 */
	private static String getNumberType(int number) {
		if (isPrim(number))
		{
			return "Prim";
		}
		if (isPar(number))
		{
			return "Par";
		}
		return "Impar";
		
	}
	
	/**
	 * Adds the new number.
	 */
	private static void addNewNumber() {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter the number you want to add: ");
		int number = sc.nextInt();
		List<model.Number> numberList;
		String numberType = getNumberType(number);
		System.out.println("The number is " + numberType);
		switch(numberType) {
		case "Prim":{
			numberList = primDao.getAll();
			primDao.create(new Number(numberList.size() + 1, number));
			break;
		}
		case "Par":{
			numberList = parDao.getAll();
			parDao.create(new Number(numberList.size() + 1, number));
			break;
		}
		case "Impar":{
			numberList = imparDao.getAll();
			imparDao.create(new Number(numberList.size() + 1, number));
			break;
		}
		}

		
	}

	/**
	 * Show impar numbers.
	 */
	private static void showImparNumbers() {
		System.out.println("Numerele impare: \n");
		printList(imparDao.getAll());
		
	}

	/**
	 * Show par numbers.
	 */
	private static void showParNumbers() {
		System.out.println("Numerele pare: \n");
		printList(parDao.getAll());
		
	}

	/**
	 * Show prime numbers.
	 */
	private static void showPrimeNumbers() {
		System.out.println("Numerele prime: \n");
		printList(primDao.getAll());
		
	}
	
	/**
	 * Prints the all.
	 */
	private static void printAll() {
		showPrimeNumbers();
		showParNumbers();
		showImparNumbers();		
	}

	/**
	 * Show page one.
	 */
	private static void showPageOne() {
		System.out.println("1. Add a new number");
		System.out.println("2. Show All Numbers");
		/*
		System.out.println("3. Show Prime Numbers");
		System.out.println("4. Show Par Numbers");
		System.out.println("5. Show Impar Numbers \n");
		*/
	}


	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws SQLException the SQL exception
	 * @throws ClassNotFoundException the class not found exception
	 */
	public static void main(String[] args) throws SQLException, ClassNotFoundException {

Menu();

	}

}