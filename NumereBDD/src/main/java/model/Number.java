package model;

import java.io.Serializable;
import javax.persistence.*;


// TODO: Auto-generated Javadoc
/**
 * The persistent class for the number database table.
 * 
 */
@Entity
@NamedQuery(name="Number.findAll", query="SELECT n FROM Number n")
public class Number implements Serializable {
	
	/**
	 * Instantiates a new number.
	 *
	 * @param id the id
	 * @param number the number
	 */
	public Number(Integer id, Integer number) {
		super();
		this.id = id;
		this.number = number;
	}

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The id. */
	@Id
	private Integer id;

	/** The number. */
	private Integer number;

	/**
	 * Instantiates a new number.
	 */
	public Number() {
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public Integer getId() {
		return this.id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Gets the number.
	 *
	 * @return the number
	 */
	public Integer getNumber() {
		return this.number;
	}

	/**
	 * Sets the number.
	 *
	 * @param number the new number
	 */
	public void setNumber(Integer number) {
		this.number = number;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Number [id=" + id + ", number=" + number + "]";
	}

}